$(document).ready(function () {
    var settings = {
        async: true,
        crossDomain: true,
        url: "https://my-json-server.typicode.com/dp-danielortiz/dptest_jsonplaceholder/items",
        method: "GET",
        data: {},
    };
    var botonDescarga = document.getElementById("guardarColorRed");

    $("#consumirJs").click(function () {
        $.ajax(settings).done(function (response) {
            // console.log(response);
            let datosJson = JSON.stringify(response, undefined, 3);
            $("#response-js").html("<pre>const dataColor =" + datosJson + ";</pre>");
            let datosColorFiltro = response.filter(
                (response) => response.color == "red"
            );
            let datosColor = JSON.stringify(datosColorFiltro, undefined, 3);
            $("#response-red").html(
                "<pre>const dataGreen = " + datosColor + ";</pre>"
            );
            console.info(datosColor);
            botonDescarga.setAttribute("style", "background-color: #EAE766");

            botonDescarga.addEventListener("click", function () {
                var dataDescarga = "data:text/json;charset=utf-8," + encodeURIComponent(datosColor);
                botonDescarga.setAttribute("href", dataDescarga);
                botonDescarga.setAttribute("download", "datosColorRed.json");
            });
        });
    });
    $("#resetear").click(function () {
        location.reload();
    });
});
